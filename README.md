# ai-security

- [OWASP AI Security Top 10](https://owasp.org/www-project-top-10-for-large-language-model-applications/descriptions/): OWASP list of the top 10 security risks associated with LLMs
- [OWASP AI Security and Privacy Guide](https://owasp.org/www-project-ai-security-and-privacy-guide/): A guide by OWASP that provides a comprehensive overview of AI security and privacy, including threats, attacks, and defenses
- [MITRE ATLAS](https://atlas.mitre.org/): The official website of MITRE's ATLAS project (Adversarial Threat Landscape for Artificial-Intelligence Systems)
- [MITRE ATLAS Mitigations](https://atlas.mitre.org/mitigations/): A subsection of the MITRE ATLAS project focusing AI risk mitigations
- [MITRE ATLAS Adversarial ML 101](https://atlas.mitre.org/resources/adversarial-ml-101): A resource from MITRE ATLAS on adversarial machine learning
- [LLM Hacker's Handbook](https://doublespeak.chat/#/handbook): empirical, non-academic, and practical guide to LLM hacking
- [Privacy Library Of Threats 4 Artificial Intelligence](https://plot4.ai/): threat modeling library to help build responsible AI
- [Plot4AI Library](https://plot4.ai/library): library of threats related to AI/ML, updated regularly
- [Plot4AI Quick Check Assessment](https://plot4.ai/assessments/quick-check)
- [Awesome AI Security](https://github.com/DeepSpaceHarbor/Awesome-AI-Security): A curated list of AI security resources
- [Gandalf](https://gandalf.lakera.ai/): Gameified Prompt Injection Attack Simulator
- [Jailbreak Chat](https://www.jailbreakchat.com/): A forum for discussing jailbreaking technologies
- [FML Security](https://github.com/EthicalML/fml-security): repository focusing on fairness, accountability, and transparency in machine learning
- [GitLab Secure Coding Guidelines for AI Features](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html#artificial-intelligence-ai-features): GitLab's guidelines for secure coding practices related to AI features
- [The AI Attack Surface Map](https://danielmiessler.com/blog/the-ai-attack-surface-map-v1-0): Presents a framework for potential vulnerabilities in AI systems
- [OWASP AI Security Top 10](https://owasp.org/www-project-top-10-for-large-language-model-applications/descriptions/): OWASP list of the top 10 security risks associated with LLMs
- [OWASP AI Security and Privacy Guide](https://owasp.org/www-project-ai-security-and-privacy-guide/): A guide by OWASP that provides a comprehensive overview of AI security and privacy, including threats, attacks, and defenses
- [Failure Modes in Machine Learning](https://learn.microsoft.com/en-us/security/engineering/failure-modes-in-machine-learning): Discusses machine learning failure modes and mitigation strategies
- [An Introduction to AI Model Security](https://www.wwt.com/article/introduction-to-ai-model-security): Explores the significance and risks of AI model security
- [AI Threats and Defenses in 2022](https://www.microsoft.com/security/blog/2022/01/25/ai-threats-and-defenses-in-2022/): Reviews AI security threats and defenses in 2022
- [Vulnerability Disclosure and Management for AI/ML Systems: A Working Paper with Policy Recommendations](https://fsi-live.s3.us-west-1.amazonaws.com/s3fs-public/ai_vuln_disclosure_nov11final-pdf_1.pdf): Provides guidelines on managing vulnerabilities in AI/ML systems
- [The Malicious Use of Artificial Intelligence: Forecasting, Prevention, and Mitigation](https://arxiv.org/pdf/1802.07228.pdf): Discusses potential malicious uses of AI and prevention strategies
- [Attacking Artificial Intelligence: AI’s Security Vulnerability and What Policymakers Can Do About It](https://www.belfercenter.org/sites/default/files/2019-08/AttackingAI/AttackingAI.pdf): Offers policy recommendations for addressing AI security vulnerabilities
- [NIST Artificial Intelligence Risk Management Framework](https://nvlpubs.nist.gov/nistpubs/ai/NIST.AI.100-1.pdf): AI risk management framework by NIST
- [Cybersecurity of AI and Standardisation](https://www.enisa.europa.eu/publications/cybersecurity-of-ai-and-standardisation): Reviews standards and gaps in AI cybersecurity
- [Toward Comprehensive Risk Assessments and Assurance of AI-Based Systems](https://github.com/trailofbits/publications/blob/master/papers/toward_comprehensive_risk_assessments.pdf)
- [Adversarial Machine Learning and Cybersecurity: Risks, Challenges, and Legal Implications](https://fsi9-prod.s3.us-west-1.amazonaws.com/s3fs-public/2023-04/adversarial_machine_learning_and_cybersecurity_v7_pdf_1.pdf): Discusses adversarial machine learning, its cybersecurity implications, and legal challenges
- [Planting Undetectable Backdoors in Machine Learning Models](https://cs.stanford.edu/~mpkim/pubs/undetectable.pdf): Discusses the concept and implications of backdoors in ML models
- [AI Technologies, Privacy, and Security](https://www.frontiersin.org/articles/10.3389/frai.2022.826737/full) Frontiers in Artificial Intelligence paper discusses impact of AI systems on key elements of privacy
- [Privacy Issues of AI](https://link.springer.com/chapter/10.1007/978-3-030-51110-4_8) an excerpt from "Introduction to Ethics in Robotics and AI" discussing privacy issues of AI
- [Pre-train, Prompt, and Predict: A Systematic Survey of Prompting Methods in Natural Language Processing](https://arxiv.org/pdf/2107.13586.pdf): Offers an in-depth study of prompting methods in NLP
- [Beyond the Safeguards: Exploring the Security Risks of ChatGPT](https://arxiv.org/pdf/2305.08005.pdf): Investigates ChatGPT security risks and enhancement strategies
- [CEPS Task Force Report: Artificial Intelligence and Cybersecurity - Technology, Governance, and Policy Challenges](https://www.ceps.eu/wp-content/uploads/2021/05/CEPS-TFR-Artificial-Intelligence-and-Cybersecurity.pdf): Outlines the technical and ethical challenges of AI and cybersecurity intersection
- [Data Privacy in AI Bill of Rights](https://www.whitehouse.gov/ostp/ai-bill-of-rights/data-privacy-2/) - US White House outlines the importance of data privacy in the context of AI, emphasizing the need for transparency, consent, and respect for user decisions regarding data collection and use
- [National Security Commission on Artificial Intelligence: Final Report](https://www.nscai.gov/wp-content/uploads/2021/03/Full-Report-Digital-1.pdf): Detailed report on AI in national security and the need for strategic investment
- [LLMs and Phishing](https://www.schneier.com/blog/archives/2023/04/llms-and-phishing.html) - Bruce Schneier discusses the potential for malicious use of language models like ChatGPT, particularly in the context of phishing attacks
- [Rise of AI Puts Spotlight on Bias in Algorithms](https://www.wsj.com/articles/rise-of-ai-puts-spotlight-on-bias-in-algorithms-26ee6cc9) - The Wall Street Journal highlights the issue of bias in AI algorithms, emphasizing the need for businesses to address this issue proactively
- [More than you’ve asked for: A Comprehensive Analysis of Novel Prompt Injection Threats to Application-Integrated Large Language Models](https://arxiv.org/pdf/2302.12173v1.pdf): research paper analyzes threats and mitigation strategies for prompt injection in large language models
- [Not what you’ve signed up for: Compromising Real-World LLM-Integrated Applications with Indirect Prompt Injection](https://arxiv.org/pdf/2302.12173.pdf): research covers risks and potential solutions for indirect prompt injection in large language model applications
- [Ignore Previous Prompt: Attack Techniques for Language Models](https://arxiv.org/pdf/2211.09527.pdf): researchers at the University of Washington and the Allen Institute for AI explores the potential for prompt injection attacks on LLMs and proposes a new attack technique called "Ignore Previous Prompt" (IPP)
- [Exploring Prompt Injection Attacks](https://research.nccgroup.com/2022/12/05/exploring-prompt-injection-attacks/): article from NCC Group on Prompt Injection Attack impact, defenses, and risk mitigation
- [The AI Act (EU)](https://artificialintelligenceact.eu/) EU's AI Act proposes to regulate AI based on risk levels
- [A European Strategy for Artificial Intelligence](https://www.ceps.eu/wp-content/uploads/2021/04/AI-Presentation-CEPS-Webinar-L.-Sioli-23.4.21.pdf) European Strategy for AI aims to regulate AI based on risk levels
- [Declassifying the Responsible Disclosure of the Prompt Injection Attack Vulnerability of GPT-3](https://www.preamble.com/prompt-injection-a-critical-vulnerability-in-the-gpt-3-transformer-and-how-we-can-begin-to-solve-it)
- [ChatGPT Data Leak Incident](https://openai.com/blog/march-20-chatgpt-outage) bug allowed users to see titles from another active user’s chat history
- [AI Security White Paper](https://www-file.huawei.com/-/media/corporate/pdf/trust-center/ai-security-whitepaper.pdf): Huawei's white paper on data security and privacy in AI
- [General Tips for Designing Prompts](https://www.promptingguide.ai/introduction/tips) Provides tips for creating effective prompts, emphasizing simplicity, specificity, and conciseness
- [Actionable Guidance for High-Consequence AI Risk Management](https://arxiv.org/pdf/2206.08966.pdf) - Discusses how to address catastrophic risks and associated issues in AI standards
- [OpenAI Safety Best Practices](https://platform.openai.com/docs/guides/safety-best-practices): A guide by OpenAI detailing best practices for AI safety, focusing on reducing harmful bias and ensuring robustness in AI systems
- [Guidance for the Development of AI Risk and Impact Assessments](https://cltc.berkeley.edu/wp-content/uploads/2021/08/AI_Risk_Impact_Assessments.pdf): A white paper by CLTC providing a framework for assessing risks and impacts of AI, emphasizing the need for transparency, accountability, and fairness
- [Explainability won't save AI](https://www.brookings.edu/techstream/explainability-wont-save-ai/): An article from Brookings arguing that explainability alone won't mitigate AI risks, and calling for comprehensive regulatory frameworks
- [Taxonomy of Trustworthiness for Artificial Intelligence](https://cltc.berkeley.edu/wp-content/uploads/2023/01/Taxonomy_of_AI_Trustworthiness.pdf): A CLTC white paper presenting a taxonomy of trustworthiness for AI, linking trustworthiness properties with risk management and AI lifecycle
- [Responsible AI Pattern Catalogue](https://research.csiro.au/ss/science/projects/responsible-ai-pattern-catalogue/): A project by CSIRO providing a catalogue of patterns for operationalizing responsible AI, covering governance, process, and product aspects
- [GPT-4 System Card](https://cdn.openai.com/papers/gpt-4-system-card.pdf) safety challenges posed by OpenAI's GPT-4 model and the measures taken to mitigate these risks
- [NIST AI Risk Management Framework](https://www.nist.gov/itl/ai-risk-management-framework): A framework by NIST for managing risks associated with AI. It includes a companion playbook, explainer video, roadmap, and more
- [NIST Artificial Intelligence Risk Management Framework](https://nvlpubs.nist.gov/nistpubs/ai/NIST.AI.100-1.pdf): AI risk management framework by NIST
- [AI RMF Knowledge Base](https://airc.nist.gov/AI_RMF_Knowledge_Base/Playbook): A playbook by NIST for AI Risk Management Framework
- [Threat Modeling AI/ML Systems and Dependencies](https://learn.microsoft.com/en-us/security/engineering/threat-modeling-aiml): Microsoft's guide to threat modeling AI/ML systems and dependencies
- [Do Users Write More Insecure Code with AI Assistants?](https://arxiv.org/pdf/2211.03622.pdf): Examines security vulnerabilities introduced by AI coding assistants
- [Hidden Poison: Machine Unlearning Enables Camouflaged Poisoning Attacks](https://arxiv.org/abs/2212.10717): Introduces camouflaged data poisoning attacks, a new threat in machine unlearning, affecting model predictions
- [Poisoning Web-Scale Training Datasets is Practical](https://arxiv.org/abs/2302.10149): Presents two new dataset poisoning attacks exploiting internet content mutability and crowd-sourced content snapshots
- [Targeted Backdoor Attacks on Deep Learning Systems Using Data Poisoning](https://arxiv.org/abs/1712.05526): Explores backdoor attacks in learning-based authentication systems, misleading the system to misclassify instances
- [Poisoning the Unlabeled Dataset of Semi-Supervised Learning](https://www.usenix.org/conference/usenixsecurity21/presentation/carlini-poisoning): Reveals vulnerabilities in semi-supervised machine learning models through poisoning attacks
- [Poisoning and Backdooring Contrastive Learning](https://arxiv.org/abs/2106.09667): Shows that backdoor and poisoning attacks pose a significant threat to multimodal contrastive learning methods
- [Manipulating Machine Learning: Poisoning Attacks and Countermeasures for Regression Learning](https://ieeexplore.ieee.org/document/8418594): Presents a study of poisoning attacks and countermeasures for linear regression models
- - [Concealed Data Poisoning Attacks on NLP Models](https://aclanthology.org/2021.naacl-main.13/): Discusses a new type of data poisoning attack that can manipulate NLP models without detection
- [Weight Poisoning Attacks on Pretrained Models](https://aclanthology.org/2020.acl-main.249/): Explores the security risks of using untrusted pre-trained weights in NLP models, demonstrating potential for "weight poisoning" attacks
- [Witches’ Brew: Industrial Scale Data Poisoning via Gradient Matching](https://arxiv.org/abs/2009.02276): Introduces a new, large-scale data poisoning attack method that can cause targeted misclassification in deep networks
- [An Equivalence Between Data Poisoning and Byzantine Gradient Attacks](https://proceedings.mlr.press/v162/farhadkhani22b.html): Shows an equivalence between data poisoning and Byzantine gradient attacks, providing new insights into the resilience of learning algorithms
- [Model-Targeted Poisoning Attacks with Provable Convergence](https://proceedings.mlr.press/v139/suya21a.html): Proposes a new model-targeted poisoning attack with provable convergence for convex models, surpassing previous attacks in success rate
- [Back to the Drawing Board: A Critical Evaluation of Poisoning Attacks on Production Federated Learning](https://ieeexplore.ieee.org/document/9833647): Evaluates the real impact of poisoning attacks on production federated learning systems, suggesting that these systems are more robust than previously believed
- [AI Bill of Rights](https://www.whitehouse.gov/ostp/ai-bill-of-rights/) - The White House presents a blueprint for an AI Bill of Rights, outlining five principles to guide the design, use, and deployment of automated systems to protect the public in the age of artificial intelligence
- [GitHub Copilot Litigation](https://githubcopilotlitigation.com/) - This website provides information about a lawsuit challenging GitHub Copilot's use of code from open-source projects
